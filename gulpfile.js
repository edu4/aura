'use strict';

/**
 *  Hosts the `dist` folder on a specific port.
 *
 *  Example Usage:
 *  gulp server
 */
var gulp = require('gulp'),
    webserver = require('gulp-webserver');

gulp.task('default', function() {
    return gulp.src('app').pipe(webserver({
        host: '0.0.0.0',
        port: process.env.PORT || 5000,
        https: false,
        open: true
    }));
});